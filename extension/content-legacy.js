let tags = [];
let imageList = [];
let remainingImages = 0;
var textContent = '';
var imageContent = '';
let orderedIndex = 0;
let origin = null;
let subOrigin = null;

// var $ = artoo.$;

const resetData = () => {
  tags = [];
  imageList = [];
  remainingImages = 0;
  textContent = '';
  imageContent = '';
  orderedIndex = 0;
  origin = null;
};

const possibleSources = [
  'qr.ae',
  'quora.com',
  'redd.it',
  'reddit.com',
  '.medium.com',  // link.medium.com, elemental.medium.com, ...
  'bit.ly',
];

// ==================== IMPORTED SCRIPT, DO NOT TOUCH ====================
// To debug on VsCode, change newline to '\n\n'
const NEWLINE = '\n\n';

// Selector
const WHOLE_POST = '._5pcr.userContentWrapper';
const POST_MESSAGE = 'div[data-testid=post_message]';

// Container classes
const POST = ['_5pbx', 'userContent' ,'_3576'];
const CONTAINER = '_3w8y';
// Single line classes
const WRAPPED_LINE = '_2cuy';
const LEFT_ALIGNED = '_2vxa';
// Inner span classes
const ITALICS = '_4yxp';
const BOLD = '_4yxo';
const EMOJI = ['_47e3', '_5mfr'];  // [title="<emotion name>"]
const EMOJI_INNER = ['_7oe'];

// Tag classes
const LINEAR = '_3dgx';
const HEADER1 = '_509y';
const HEADER2 = '_50a1';
const LIST = '_5a_q';
const ORDERED = '_509r';
const UNORDERED = '_5yj1';
const LIST_ITEM_UNORDERED = '_509q';
const LIST_ITEM_ORDERED = '_509s';
const BLOCKQUOTE = '_509u';
const HASHTAG = '_58cn';
const LINK = '_2u0z';

// Reaction and comments
const REACTION = '._66lg>a._3dlf>span._3dlg>span._3dlh>span._81hb';
const REACTION_DETAILS = 'a._1n9l';
const COMMENT_COUNT = 'a._3hg-._42ft';


// l.facebook.com Regex
const L_FB_REGEX = /https:\/\/l.facebook.com\/l.php\?u=(http[s]?%3A%2F%2F.+)%3F.*/;


/**
 * Image fetching
 * Following what the content offers, it's able to get the image only.
 * The image URL can be accessed without any access token, the order of prioritized URLs is as follows:
 * - a[rel=theater] => attr('data-ploi')
 * - a[rel=theater] => attr('data-plsi')  // The s960x960 one
 * - img._46-i.img => attr('data-src)
 */
const IMAGE_MULTIPLE_CONTAINER = ['_2a2q', '_65sr'];
const IMAGE_SINGLE_CONTAINER = ['_5cq3', '_1ktf'];
const IMAGE_MULTIPLE_WRAPPER = ['_5dec', '_xcx', '_487t'];  // <a rel="theater">
const IMAGE_SINGLE_WRAPPER = ['_4-eo', '_2t9n', '_50z9'];
const IMAGE_CROPPER = ['_46-h', '_4-ep', 'uiScaledImageContainer', '_517g'];  // The former is the main one, the smaller images in theater might have 'uiScaledImageContainer' class, sometimes we've got _517g
const IMAGE_INNER = ['_46-i', 'img', 'scaledImageFitHeight'];  // The smaller images in theater might have 'scaledImageFitHeight' class
const MORE_IMAGES = '_52db';
const TIMESTAMP_CONTAINER = '_5pcq';
const TIMESTAMP = '_5ptz';

const ET = {
  POST: 'POST',
  CONTAINER: 'CONTAINER',
  LINEAR: 'LINEAR',
  PARAGRAPH: 'PARAGRAPH',
  HEADER1: 'HEADER1',
  HEADER2: 'HEADER2',
  BLOCKQUOTE: 'BLOCKQUOTE',
  LIST_ITEM_UNORDERED: 'LIST_ITEM_UNORDERED',
  LIST_ITEM_ORDERED: 'LIST_ITEM_ORDERED',
  LIST_ORDERED: 'LIST_ORDERED',
  LIST_UNORDERED: 'LIST_UNORDERED',
  SPAN_BOLD: 'SPAN_BOLD',
  SPAN_ITALICS: 'SPAN_ITALICS',
  SPAN_BOLD_ITALICS: 'SPAN_BOLD_ITALICS',
  SPAN_EMOJI: 'SPAN_EMOJI',
  SPAN_EMOJI_INNER: 'SPAN_EMOJI_INNER',
  LINK: 'LINK',
  LINK_HASHTAG: 'LINK_HASHTAG',
  LINEBREAK: 'LINEBREAK',
  IMAGE_MULTIPLE_CONTAINER: 'IMAGE_MULTIPLE_CONTAINER',
  IMAGE_MULTIPLE_WRAPPER: 'IMAGE_MULTIPLE_WRAPPER',
  IMAGE_SINGLE_WRAPPER: 'IMAGE_SINGLE_WRAPPER',
  IMAGE_SINGLE_CONTAINER: 'IMAGE_SINGLE_CONTAINER',
  IMAGE_CROPPER: 'IMAGE_CROPPER',
  IMAGE_INNER: 'IMAGE_INNER',
  MORE_IMAGES: 'MORE_IMAGES',
  TIMESTAMP_CONTAINER: 'TIMESTAMP_CONTAINER',
  TIMESTAMP: 'TIMESTAMP',
  EDITOR: 'EDITOR',
  WHOLE_POST: 'WHOLE_POST',
  REACTION: 'REACTION',
  REACTION_DETAILS: 'REACTION_DETAILS',
  COMMENT_COUNT: 'COMMENT_COUNT',
};

const FB_TAG = {
  [ET.POST]: 'DIV',
  [ET.CONTAINER]: 'DIV',
  [ET.LINEAR]: 'DIV',
  [ET.PARAGRAPH]: 'P',
  [ET.HEADER1]: 'H2',
  [ET.HEADER2]: 'H3',
  [ET.BLOCKQUOTE]: 'BLOCKQUOTE',
  [ET.LIST_ITEM_UNORDERED]: 'LI',
  [ET.LIST_ITEM_ORDERED]: 'LI',
  [ET.LIST_ORDERED]: 'OL',
  [ET.LIST_UNORDERED]: 'UL',
  [ET.SPAN_BOLD]: 'SPAN',
  [ET.SPAN_ITALICS]: 'SPAN',
  [ET.SPAN_BOLD_ITALICS]: 'SPAN',
  [ET.SPAN_EMOJI]: 'SPAN',
  [ET.SPAN_EMOJI_INNER]: 'SPAN',
  [ET.LINK]: 'A',
  [ET.LINK_HASHTAG]: 'A',
  [ET.LINEBREAK]: 'BR',
  [ET.IMAGE_MULTIPLE_CONTAINER]: 'DIV',
  [ET.IMAGE_SINGLE_CONTAINER]: 'DIV',
  [ET.IMAGE_MULTIPLE_WRAPPER]: 'A',
  [ET.IMAGE_SINGLE_WRAPPER]: 'A',
  [ET.IMAGE_CROPPER]: 'DIV',
  [ET.IMAGE_INNER]: 'IMG',
  [ET.MORE_IMAGES]: 'DIV',
  [ET.TIMESTAMP_CONTAINER]: 'A',
  [ET.TIMESTAMP]: 'ABBR',
};

const FB_CLS = {
  [ET.POST]: POST,
  [ET.CONTAINER]: [CONTAINER],
  [ET.LINEAR]: [WRAPPED_LINE, LINEAR, LEFT_ALIGNED],
  [ET.PARAGRAPH]: [],
  [ET.HEADER1]: [WRAPPED_LINE, HEADER1, LEFT_ALIGNED],
  [ET.HEADER2]: [WRAPPED_LINE, HEADER2, LEFT_ALIGNED],
  [ET.BLOCKQUOTE]: [WRAPPED_LINE, BLOCKQUOTE, LEFT_ALIGNED],
  [ET.LIST_ITEM_UNORDERED]: [WRAPPED_LINE, LIST_ITEM_UNORDERED, LEFT_ALIGNED],
  [ET.LIST_ITEM_ORDERED]: [WRAPPED_LINE, LIST_ITEM_ORDERED, LEFT_ALIGNED],
  [ET.LIST_ORDERED]: [LIST, ORDERED],
  [ET.LIST_UNORDERED]: [LIST, UNORDERED],
  [ET.SPAN_BOLD]: [BOLD],
  [ET.SPAN_ITALICS]: [ITALICS],
  [ET.SPAN_BOLD_ITALICS]: [BOLD, ITALICS],
  [ET.SPAN_EMOJI]: EMOJI,
  [ET.SPAN_EMOJI_INNER]: EMOJI_INNER,
  [ET.LINK]: [LINK],
  [ET.LINK_HASHTAG]: [HASHTAG],
  [ET.LINEBREAK]: [],
  [ET.IMAGE_MULTIPLE_CONTAINER]: IMAGE_MULTIPLE_CONTAINER,
  [ET.IMAGE_SINGLE_CONTAINER]: IMAGE_SINGLE_CONTAINER,
  [ET.IMAGE_MULTIPLE_WRAPPER]: IMAGE_MULTIPLE_WRAPPER,
  [ET.IMAGE_SINGLE_WRAPPER]: IMAGE_SINGLE_WRAPPER,
  [ET.IMAGE_CROPPER]: IMAGE_CROPPER,
  [ET.IMAGE_INNER]: IMAGE_INNER,
  [ET.MORE_IMAGES]: [MORE_IMAGES],
  [ET.TIMESTAMP_CONTAINER]: [TIMESTAMP_CONTAINER],
  [ET.TIMESTAMP]: [TIMESTAMP],
};

const FB_SEL = {
  [ET.POST]: `${POST_MESSAGE}`,
  [ET.IMAGE_CONTAINER]: '.mtm',
  [ET.CONTAINER]: `.${CONTAINER}`,
  [ET.LINEAR]: `.${WRAPPED_LINE}.${LINEAR}.${LEFT_ALIGNED}`,
  [ET.PARAGRAPH]: `p`,
  [ET.HEADER1]: `h2.${WRAPPED_LINE}.${HEADER1}.${LEFT_ALIGNED}`,
  [ET.HEADER2]: `h3.${WRAPPED_LINE}.${HEADER2}.${LEFT_ALIGNED}`,
  [ET.BLOCKQUOTE]: `blockquote.${WRAPPED_LINE}.${BLOCKQUOTE}.${LEFT_ALIGNED}`,
  [ET.LIST_ITEM_UNORDERED]: `li.${WRAPPED_LINE}.${LIST_ITEM_UNORDERED}.${LEFT_ALIGNED}`,
  [ET.LIST_ITEM_ORDERED]: `li.${WRAPPED_LINE}.${LIST_ITEM_ORDERED}.${LEFT_ALIGNED}`,
  [ET.LIST_ORDERED]: `ol.${LIST}.${ORDERED}`,
  [ET.LIST_UNORDERED]: `ul.${LIST}.${UNORDERED}`,
  [ET.SPAN_BOLD]: `span.${BOLD}`,
  [ET.SPAN_ITALICS]: `span.${ITALICS}`,
  [ET.SPAN_BOLD_ITALICS]: `span.${BOLD}.${ITALICS}`,
  [ET.SPAN_EMOJI]: `span.${EMOJI.join('.')}`,
  [ET.SPAN_EMOJI_INNER]: `span.${EMOJI_INNER.join('.')}`,
  [ET.LINK]: ``,
  [ET.LINK_HASHTAG]: `a.${HASHTAG}`,
  [ET.LINEBREAK]: `br`,
  [ET.IMAGE_MULTIPLE_CONTAINER]: `.${IMAGE_MULTIPLE_CONTAINER.join('.')}`,
  [ET.IMAGE_SINGLE_CONTAINER]: `${IMAGE_SINGLE_CONTAINER.join('.')}`,
  [ET.IMAGE_MULTIPLE_WRAPPER]: `a.${IMAGE_MULTIPLE_WRAPPER.join('.')}[rel=theater]`,
  [ET.IMAGE_SINGLE_WRAPPER]: `a.${IMAGE_SINGLE_WRAPPER.join('.')}[rel=theater]`,
  [ET.IMAGE_CROPPER]: `.${IMAGE_CROPPER.join('.')}`,
  [ET.IMAGE_INNER]: `img.${IMAGE_INNER.join('.')}`,
  [ET.MORE_IMAGES]: `._52d9>._52da`,  // >._52db
  [ET.TIMESTAMP_CONTAINER]: `a.${TIMESTAMP_CONTAINER}`,  // >abbr._5ptz
  [ET.TIMESTAMP]: `abbr.${TIMESTAMP}`,
  [ET.EDITOR]: `.fwb.fcg[data-ft=\\{\\"tn\\"\\:\\"\\;\\"\\}]>a`,
  [ET.WHOLE_POST]: WHOLE_POST,
  [ET.REACTION]: REACTION,
  [ET.REACTION_DETAILS]: REACTION_DETAILS,
  [ET.COMMENT_COUNT]: COMMENT_COUNT,
};

// Markdown types
const MT = {
  LINEAR: 'LINEAR',
  BOLD: 'BOLD',
  ITALICS: 'ITALICS',
  BOLD_ITALICS: 'BOLD_ITALICS',
  LIST_ITEM_UNORDERED: 'LIST_ITEM_UNORDERED',
  LIST_ITEM_ORDERED: 'LIST_ITEM_ORDERED',
  LIST_ORDERED: 'LIST_ORDERED',
  LIST_UNORDERED: 'LIST_UNORDERED',
  LINK: 'LINK',
  NUMBERED_LIST: 'NUMBERED_LIST',  // unused
  QUOTE: 'QUOTE',
  H1: 'H1',
  H2: 'H2',
  INLINE_CODE : 'INLINE_CODE',
  LINEBREAK: 'LINEBREAK',
  IMAGE_FULL: 'IMAGE_FULL',
  IMAGE: 'IMAGE',
  HIDDEN: 'HIDDEN',
};


const mapElementToMarkdown = {
  [ET.LINEAR]: MT.LINEAR,
  [ET.HEADER1]: MT.H1,
  [ET.HEADER2]: MT.H2,
  [ET.BLOCKQUOTE]: MT.QUOTE,
  [ET.LIST_ITEM_UNORDERED]: MT.LIST_ITEM_UNORDERED,
  [ET.LIST_ITEM_ORDERED]: MT.LIST_ITEM_ORDERED,
  [ET.LIST_ORDERED]: MT.LIST_ORDERED,
  [ET.LIST_UNORDERED]: MT.LIST_UNORDERED,
  [ET.SPAN_BOLD]: MT.BOLD,
  [ET.SPAN_BOLD_ITALICS]: MT.BOLD_ITALICS,
  [ET.SPAN_ITALICS]: MT.ITALICS,
  [ET.SPAN_EMOJI_INNER]: MT.HIDDEN,
  [ET.LINK]: MT.LINK,
  [ET.LINK_HASHTAG]: MT.LINK,
  [ET.LINEBREAK]: MT.LINEBREAK,
  [ET.IMAGE_MULTIPLE_WRAPPER]: MT.IMAGE_FULL,
  [ET.IMAGE_SINGLE_WRAPPER]: MT.IMAGE_FULL,
  [ET.IMAGE_INNER]: MT.IMAGE,
};


/**
 * @description Format the given content by adding characters according to the given type.
 * The bold and italics format has some variant, can be either ***, ___, __* or **_
 * @param {String} type - Indicate which actions the method is gonna perform
 * @param {String} content - The content to format
 * @param {String} sub - The substitution if any
 */
const format = (type, content = null, sub = null) => {
  if (!type) {
    return content;
  }

  if (type === MT.LINEBREAK) {
    return NEWLINE;
  }

  content = content.trim();
  if (!content && (type !== MT.IMAGE_FULL && type !== MT.IMAGE)) {
    return '';
  }


  switch(type) {
    case MT.LINEAR:
      /**
       * Adding a linebreak after a linear is tricky, as the editor may unintentionally
       * add his linebreak already.
       * A workaround should be to disable every standalone <br> in a linear div tag.
       */
      // return `${content}\n`;
      return `${content}`;
    case MT.BOLD:
      return `**${content}**`;
    case MT.ITALICS:
      return `*${content}*`;
    case MT.BOLD_ITALICS:
      return `***${content}***`;
    case MT.LIST_ITEM_UNORDERED:
      return `- ${content}`;
    case MT.LIST_ITEM_ORDERED:
      return `${++orderedIndex}. ${content}`;
    case MT.LIST_ORDERED:
      // orderedIndex = 0;
      // return `${content}${NEWLINE}`;
      return `${content}`;
    case MT.LIST_UNORDERED:
      // return `${content}${NEWLINE}`;
      return `${content}`;
    case MT.LINK:
      // Judge sub origin
      if (sub.startsWith('/')) {
        sub = `https://www.facebook.com${sub}`;
      } else if (sub.startsWith('https://www.facebook.com')) {
        sub = sub.split('?')[0];
      } else {
        let lfb = L_FB_REGEX.exec(sub);
        if (lfb) {
          sub = decodeURIComponent(lfb[1]);
        }
      }

      if (!origin) {
        let pattern = ORIGIN_REGEX.exec(sub);
        if (pattern) {
          let endpoint = pattern[2];
          // Ignore medium users
          if (!pattern[3].includes('@')) {
            subOrigin = subOrigin || (judgeOrigin(endpoint) ? sub : null);
          }
        }
        pattern = MEDIUM_REGEX.exec(sub);
        if (pattern) {
          console.log(`##### PATTERN DETECTED`);
          console.log(pattern);
          console.log(`##### PATTERN DETECTED`);
          subOrigin = subOrigin || sub;
        }
      }
      return `[${content}](${sub})`;
    case MT.NUMBERED_LIST:
      return `${sub || 1}. ${content}`;
    case MT.QUOTE:
      // return `> ${content}${NEWLINE}`;
      return `> ${content}`;
    case MT.H1:
      return `# ${content}`;
    case MT.H2:
      // return `## ${content.toUpperCase()}`;
      return `## ${content}`;
    case MT.INLINE_CODE:
      return `\`${content}\``;
    case MT.IMAGE_FULL:
      imageList.push({
        full: sub.dataPloi,
      });
      return `${content}`;
      break;
    case MT.IMAGE:
      return `![${sub.alt}](${sub.dataSrc})`;
    case MT.HIDDEN:
      return '';
    default:
      return content;
  }
};

const isSubsetOf = (smaller, bigger) => smaller.every(single => bigger.includes(single));
const judgeElements = (tag, classes) => {
  if (tag === 'BR') {
    return ET.LINEBREAK;
  }

  let strippedClasses = classes.filter( _c => (_c !== WRAPPED_LINE || _c !== LEFT_ALIGNED));
  if ((!strippedClasses || !strippedClasses.length) && tag !== 'A' && tag !== 'P') {
    // Only 'A' tag is allowed to have no classes
    return null;
  }

  for (let type of Object.keys(ET)) {
    if (tag === FB_TAG[type] && isSubsetOf(strippedClasses, FB_CLS[type])) {
      return type;
    }
  }
};

const judgeOrigin = url => {
  return possibleSources.find(source => url.includes(source));
};

const isInline = elem => (elem === ET.LINEAR || elem === ET.HEADER1 || elem === ET.HEADER2 || elem == ET.LIST_ITEM_UNORDERED || elem == ET.LIST_ITEM_ORDERED || elem == ET.BLOCKQUOTE || elem === ET.PARAGRAPH);
const isLink = elem => (elem === ET.LINK || elem === ET.LINK_HASHTAG);
const isSpan = elem => (elem === ET.SPAN_BOLD || elem === ET.SPAN_ITALICS || elem === ET.SPAN_EMOJI);
const isImage = elem => (elem === ET.IMAGE_INNER);
const isImageContainer = elem => (elem === ET.IMAGE_MULTIPLE_WRAPPER || elem === ET.IMAGE_SINGLE_WRAPPER);
const isList = elem => (elem === ET.LIST_ORDERED || elem === ET.LIST_UNORDERED);

function retrieve($node, addNewline = true) {
  let nodeContents = [];
  let children = $node.contents();
  // console.log(`[retrieve] children = ${JSON.stringify(children)}`);

  if (!children.length) {
    let rawContent = $node.text().trim()
      .replace(/\*/g, '\\*')
      .replace(/\>/g, '\\>');
    if (rawContent.startsWith('#') && rawContent.length < 20) {
      tags.push(rawContent);
    }
    let pattern = ORIGIN_REGEX.exec(rawContent);
    if (pattern) {
      let endpoint = pattern[2];
      // Ignore medium users
      if (!pattern[3].includes('@')) {
        origin = origin || (judgeOrigin(endpoint) ? rawContent : null);
      }
    }
    pattern = MEDIUM_REGEX.exec(rawContent);
    if (pattern) {
      origin = origin || rawContent;
    }
    return rawContent;
  };

  children.each(function(i) {
    let child = $(this);
    let classes = child.attr('class');
    // console.log(`[each] classes = ${classes}`);
    if (classes) {
      classes = classes.split(' ');
    } else {
      classes = [];
    }
    let tagName = child.prop('tagName');
    // console.log(`[each] tagName = ${tagName}`);

    let sub = null;
    let element = judgeElements(tagName, classes);
    // console.log(`[each] element = ${element}`);

    let retrieved = retrieve(child, addNewline && !isInline(element));
    // if (element) {
    //   console.log(`[each] element = ${element} => new line = ${addNewline && !isInline(element)}`);
    //   console.log(`[each] retrieved = ${retrieved}`);
    // }

    let formatType = element ? mapElementToMarkdown[element] : null;

    if ((isSpan(element) || isInline(element)) && retrieved.trim().includes(NEWLINE)) {
      let shards = retrieved.split(NEWLINE);
      let formattedShards = shards.map(shard => format(formatType, shard));
      nodeContents.push(formattedShards.join(NEWLINE));
    } else {
      if (isLink(element)) {
        sub = child.attr('href');
      } else if (isImage(element)) {
        sub = {
          dataSrc: child.attr('data-src') || child.attr('src'),
          alt: child.attr('alt'),
        };
      } else if (isImageContainer(element)) {
        sub = {
          dataPloi: child.attr('data-ploi'),
        };
      }
      nodeContents.push(format(formatType, retrieved, sub));
    }

    if (isList(element)) {
      orderedIndex = 0;
    }
  });

  return nodeContents.join(addNewline ? NEWLINE : ' ');
}
// ==================== IMPORTED SCRIPT, DO NOT TOUCH ====================

const MSG = {
  CLICKED_BROWSER_ACTION: 'CLICKED_BROWSER_ACTION',
  FETCH_ADDITIONAL_IMAGES: 'FETCH_ADDITIONAL_IMAGES',
  FINALIZE_CONTENT: 'FINALIZE_CONTENT',
  SAVED_CONTENT: 'SAVED_CONTENT',
};

const INFO = (requestOrId, message, payload) => {
  let _i = null, _m = null, _p = null;
  if (typeof requestOrId === 'object') {
    _i = requestOrId.id;
    _m = requestOrId.message;
    _p = requestOrId.payload;
  } else {
    _i = requestOrId;
    _m = message;
    _p = payload;
  }

  console.log(`[content] Received message ${_m} with id = ${_i} and payload = ${JSON.stringify(_p)}`);
};

// Regexes
const GROUP_POST_REGEX = /https:\/\/www.facebook.com\/groups\/(.*)\/permalink\/(\d*)\/*/;
const STYLE_REGEX = /background-image: url\(\'([^\(\']*)\'\);/;
const IMAGE_URL_REGEX = /\\(.{2}) {1}/g;
const ORIGIN_REGEX = /^(https:\/\/|http:\/\/)?([A-Za-z0-9.]+)(\/[^\/]*){1}$/;
const MEDIUM_REGEX = /^https:\/\/medium.com(\/[^\/]*){2}$/;

// Substitutions
const IMAGE_URL_SUB = `%$1`;

const mapGroupToCategory = {
  'vietnamquora': 'quora',
  'redditvietnam': 'reddit',
  '1988191741413952': 'quora',
  '366378530426222': 'reddit',
};

const getId = {
  'vietnamquora': 1988191741413952,
  'redditvietnam': 366378530426222,
};

const output = {
  origin: '',  // The original link of this facebook post
  translationOrigin: '', // The link of this facebook post
  translator: '',
  groupId: '',
  postId: '',
  format: 'md',  // Most of the time 'md' = markdown, sometimes 'plain'
  category: '',  // 'reddit', 'quora', 'medium' or something else
  collection: '',  // 'Future Attendance', 'Cognition', etc. User defined
  message: '',  // The full content of this facebook post (text + parsed images)
  images: imageList,  //  Any images associated that are fetchable
  tags: tags,  // Default tags and additional by editors
  remainingImages: 0,  // If > 0, there are some missing images that have to be fetched manually
  timestamp: 0,
  timestampUTC: '',
  contributor: '',
  reacted: { },
};


/**
 * The process of generating content consists of these following steps:
 * click browser action -> (crawl data from the html) -> fetch additional images -> finalize (generate) content -> save content
 * The current URL of the content script can be retrieved using `location.href`, whereas in the background script, it can query for the current active tab and extract its url.
 */
chrome.runtime.onMessage.addListener(
  (request, sender, sendResponse) => {
    INFO(request);
    if (request.message === MSG.CLICKED_BROWSER_ACTION) {
      resetData();

      let groupAndPostPattern = GROUP_POST_REGEX.exec(request.payload);
      if (groupAndPostPattern) {
        output.groupId = groupAndPostPattern[1];
        output.groupId = getId[output.groupId] || output.groupId;
        output.postId = +groupAndPostPattern[2];
        output.category = mapGroupToCategory[output.groupId];
        output.translationOrigin = request.payload;
      }

      $(document).find(FB_SEL[ET.EDITOR]).each(function() {
        let $container = $(this);
        output.translator = $container.text();
      });

      $(document).find(FB_SEL[ET.TIMESTAMP]).each(function() {
        let $container = $(this);
        output.timestamp = +$container.attr('data-utime');
        output.timestampUTC = $container.attr('title');
      });

      $(document).find(FB_SEL[ET.POST]).each(function() {
        let $container = $(this);
        console.log($container);
        textContent = retrieve($container);
        console.log(`[${MSG.CLICKED_BROWSER_ACTION}]: textContent = ${textContent}`);
      });

      $(document).find(FB_SEL[ET.REACTION]).each(function() {
        let $container = $(this);
        output.reacted.total = parseInt($container.text());
      });

      $(document).find(FB_SEL[ET.REACTION_DETAILS]).each(function() {
        let $container = $(this);
        console.log($container);
        let valueAndKey = $container.attr('aria-label').split(' ');
        output.reacted[valueAndKey[1].toLowerCase()] = parseInt(valueAndKey[0]);
      });

      $(document).find(FB_SEL[ET.COMMENT_COUNT]).each(function() {
        let $container = $(this);
        output.commentCount = parseInt($container.text().split(' ')[0]);
      });

      $(document).find(FB_SEL[ET.MORE_IMAGES]).each(function() {
        let $container = $(this);
        remainingImages = +$container.text();
      });

      if (!remainingImages) {
        console.log(`[${MSG.CLICKED_BROWSER_ACTION}] No remaining image`);
        $(document).find(FB_SEL[ET.IMAGE_CONTAINER]).each(function() {
          let $container = $(this);
          console.log($container);
          retrieve($container);
          let convertedImageList = imageList.map(
            image => format(MT.IMAGE, '', { dataSrc: image.full, alt: 'Image' })
          );
          output.images = imageList.map(_img => ({ dataSrc: _img.full, alt: 'Image' }));
          imageContent = convertedImageList.join('\n\n');
          console.log(`[${MSG.CLICKED_BROWSER_ACTION}]: imageContent = ${imageContent}`);
        });
      }


      chrome.runtime.sendMessage(
        {
          id: request.id,
          message: MSG.FETCH_ADDITIONAL_IMAGES,
          payload: remainingImages ? output.postId : '',
        },
      );
    } else if (request.message === MSG.FINALIZE_CONTENT) {
      INFO(request);
      if (request.payload && request.payload.length) {
        console.log(request.payload);
        let images = request.payload.map(
            _img => {
              let href = null;
              let urlPattern = STYLE_REGEX.exec(_img.style);
              if (urlPattern.length && urlPattern[1]) {
                href = urlPattern[1].replace(IMAGE_URL_REGEX, IMAGE_URL_SUB);
              }
              return {
                alt: _img.alt,
                dataSrc: decodeURIComponent(href),
              };
            }
          )
          .filter(_img => _img.dataSrc)
          .map(
            _img => format(MT.IMAGE, '', _img)
          );

        if (!!images.length) {
          output.images = images;
        }
        imageContent = images.join('\n\n');
      }

      console.log(`[${MSG.FINALIZE_CONTENT}]: textContent = ${textContent}`);
      console.log(`[${MSG.FINALIZE_CONTENT}]: imageContent = ${imageContent}`);
      console.log(`[${MSG.FINALIZE_CONTENT}]: origin = ${origin}`);
      console.log(`[${MSG.FINALIZE_CONTENT}]: subOrigin = ${subOrigin}`);

      origin = origin || subOrigin;
      output.origin = origin;
      output.tags = tags.map(_t => _t.substring(1));
      output.message = [textContent, imageContent].join('\n\n\n\n');
      output.textContent = textContent;
      output.imageContent = imageContent;
      if (origin && origin.includes('medium.com')) {
        output.category = "medium";
      }

      artoo.savePrettyJson(output, {
        filename: `${output.groupId}_${output.postId}.json`,
      });

      chrome.runtime.sendMessage(
        {
          id: request.id,
          message: MSG.SAVED_CONTENT,
          payload: output,
        },
      );
    }
  }
);
