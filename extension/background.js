const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1ZDVjZjg0OGY4NDY2NzQxNjhhYzRlM2EiLCJ1c2VybmFtZSI6InJlbXkuc3ByZWFkd2luZ3MiLCJlbWFpbCI6InNwcmVteTE5OTNAZ21haWwuY29tIiwiaWF0IjoxNTgxNDY5NDgxLCJleHAiOjE1ODE0NzY2ODF9.vOYnQ8k29Ww7ERg0dmAhVg1L99DlRLwOrAdq7mOADgI";

const MSG = {
  CLICKED_BROWSER_ACTION: 'CLICKED_BROWSER_ACTION',
  FETCH_ADDITIONAL_IMAGES: 'FETCH_ADDITIONAL_IMAGES',
  FINALIZE_CONTENT: 'FINALIZE_CONTENT',
  SAVED_CONTENT: 'SAVED_CONTENT',
};

const INFO = (requestOrId, message, payload) => {
  let _i = null, _m = null, _p = null;
  if (typeof requestOrId === 'object') {
    _i = requestOrId.id;
    _m = requestOrId.message;
    _p = requestOrId.payload;
  } else {
    _i = requestOrId;
    _m = message;
    _p = payload;
  }

  console.log(`[background] Received message ${_m} with id = ${_i} and payload = ${JSON.stringify(_p)}`);
};

chrome.browserAction.onClicked.addListener(
  tab => {
    chrome.tabs.query({
      active: true,
      currentWindow: true,
      lastFocusedWindow: true,
    }, tabs => {
      let activeTab = tabs[0];
      console.log(`Sending message ${MSG.CLICKED_BROWSER_ACTION} to tab [${activeTab.id}]`);
      chrome.tabs.sendMessage(
        activeTab.id,
        {
          id: activeTab.id,
          message: MSG.CLICKED_BROWSER_ACTION,
          payload: activeTab.url,
        }
      );
    });
  }
);

// This block is new!
chrome.runtime.onMessage.addListener(
  (request, sender, sendResponse) => {
    if (request.message === MSG.FETCH_ADDITIONAL_IMAGES) {
      const { id, message, payload } = request;
      const fullImageList = [];
      INFO(request);
      // FETCH_ADDITIONAL_IMAGES logic
      if (request.payload) {
        chrome.tabs.create(
          {
            url: `https://www.facebook.com/media/set/?set=pcb.${payload}`,
          },
          tab => {
            chrome.tabs.executeScript(
              tab.id,
              // { code: 'document.body.innerText' },
              { file: 'additional.js' },
              results => {
                chrome.tabs.sendMessage(
                  id,
                  {
                    id,
                    message: MSG.FINALIZE_CONTENT,
                    payload: results[0],
                  }
                );
              }
            );
          }
        );
      } else {
        chrome.tabs.sendMessage(
          id,
          {
            id,
            message: MSG.FINALIZE_CONTENT,
            payload: null,
          }
        );
      }
      // FETCH_ADDITIONAL_IMAGES logic
    } else if (request.message === MSG.SAVED_CONTENT) {
      // SAVED_CONTENT logic
      console.log(request.payload);
      console.log(request.payload.message);

      // const requestConfigs = {
      //   "async": true,
      //   "url": "http://localhost:3034/reminis-api/reader/post-message",
      //   "method": "POST",
      //   "headers": {
      //     "Content-Type": "application/json",
      //     "Authorization": `Bearer ${token}`,
      //   },
      //   "data": JSON.stringify(request.payload),
      //   // "data": request.payload,
      // };

      // $.ajax(requestConfigs)
      //   .done(function (data) {
      //     console.log(`Done with result = ${data.success}`);
      //   })
      //   .fail(function (error) {
      //     console.log(`Failed with error = ${JSON.stringify(error)}`);
      //   })
      //   .always(function () {
      //     console.log(`Always triggered!`);
      //   });
      // SAVED_CONTENT logic
    }
  }
);
