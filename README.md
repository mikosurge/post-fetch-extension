# Google Chrome post-fetch extension
An extension on Google Chrome to fetch knowledge posts.


## Overview
This extension supports scraping post content from social networks such as Facebook, Quora, and Medium.
As these sites don't have or removed appropriate APIs to fetch its post content and its content is dynamically generated, the main idea is to inject a client-side web scraper (like artoo.js), read pre-defined tag classes for header, content, and images.

Injecting web scraper to a site is tricky, as you need to bypass the Content Security Policy. Thanks to the corresponding extension on Google Chrome, the problem is solved.

## Acknowledgement
### Building an extension
Constructing a new extension for Google Chrome can be quite awkward to beginner. Several "Extension 101" guidances are provided here to make sure you are good to go as soon as possible.


- [The official documentation:](https://developer.chrome.com/extensions/getstarted) For all advanced options
- [Setting up a simple UI:](https://www.sitepoint.com/create-chrome-extension-10-minutes-flat/) Adding html and js source to the extension
- [Content and background scripts:](https://thoughtbot.com/blog/how-to-make-a-chrome-extension) The limitation of context, communication between script contexts, along with URL patterns
- [Some more interesting content.](https://usersnap.com/blog/develop-chrome-extension/)

### Simulation
To make it easy to debug, all walks of content are constantly added to the "samples" folder, which will be simulated using Node.js jsdom package.

The scraper component is set up separately from the extension component, in which a simple command can be run to parse all given HTML content to markdown format.

Simply `node index.js > output.md` to test the output.

### Facebook post
Facebook's posts have similar classes for common elements. Getting all element classes does help to rebuild the full content of the post. As the classes are automatically generated, it can be changed in the future.
The script tries to recognize every known Facebook element it could, then reconstruct it in the markdown format.

The table below shows some entry point to get all essential parts of a Facebook, by selecting correct tags using the given selectors:


|Component|Use|Selector|
|:-|:-|:-|
|Header|Fetch the translator's name and the timestamp when the post is posted||
|Post message|The main text content of the post|`.5pbx.userContent._3576`|
|Images|Images attached to the text content|`._3x-2>div[data-ft^=\\{\\"tn\\"\\:\\"H\\"\\}]`|

Upon completion, the result is returned containing these fields:


|Field|Description|
|:-|:-|
|origin|The original link of this facebook post|
|translatedOrigin|The link of this facebook post|
|translator|The name of who translated it|
|groupId|The facebook group ID that the post belongs to|
|postId|The facebook ID of this facebook post|
|format|The format of the text content in this facebook post, `md` by default|
|category|The category of this facebook, based on its origin|
|collection|The collection that it's in, based on what it's about|
|message|The full converted content of this facebook post|
|images|The list of images used in this facebook post|
|tags|All tags detected in the post|
|remainingImages|Remaining images other than what have been shown|

In the very case of too many images, it is impossible to get all original image links at once so it has to be another process to fetch these links by visiting this given URL:

`https://www.facebook.com/media/set/?set=pcb.<groupId>`

The URL is used for every Facebook's post, provides all images and links to the best resolution.

## Functionalities
|Platform|Functionality|
|:-|:-|
|Facebook|Fetch its post (header, body, images) and reconstruct the original markdown content|
|Reddit|Not supported|
|Quora|Not supported|
|Medium|Not supported|
