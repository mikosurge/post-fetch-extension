const gqlGetTop = {
  operationName: "getTop",
  variables: {
    first: 5,
    since: "2020-02-07T17:00:00.000Z",
    until: null,
  },
  query: `query getTop($first: Int, $since: Date, $until: Date) {
    likes: posts(orderBy: likes_count_DESC, first: $first, filter: {since: $since, until: $until}) {
      edges {
        node {
          r
          id
          raw_id
          comments_count
          message
          created_time
          likes_count
          attachments
          is_deleted
          status
          is_hide
          user {
            id
            raw_id
            name
            profile_pic
            nickname
            bio
            __typename
          }
          __typename
        }
        __typename
      }
      __typename
    }
    comments: posts(orderBy: comments_count_DESC, first: $first, filter: {since: $since, until: $until}) {
      edges {
        node {
          r
          id
          raw_id
          comments_count
          message
          created_time
          likes_count
          attachments
          is_deleted
          status
          is_hide
          user {
            id
            raw_id
            name
            profile_pic
            nickname
            bio
            __typename
          }
          __typename
        }
        __typename
      }
      __typename
    }
    top {
      posts_count(first: $first, since: $since, until: $until) {
        edges {
          node {
            id
            raw_id
            name
            profile_pic
            posts_count
            comments_count
            __typename
          }
          __typename
        }
        __typename
      }
      __typename
    }
  }`,
};
