module.exports.gqlGetHomeInfo = {
  operationName: "getHomeInfo",
  variables: {
  },
  query: `query getHomeInfo {
    usersCount: count(type: USERS)
    postsCount: count(type: POSTS)
    commentsCount: count(type: COMMENTS)
    subreddits: subreddits(first: 2000) {
      pageInfo {
        hasNextPage
        endCursor
        __typename
      }
      totalCount
      edges {
        node {
          raw_id
          posts_count
          __typename
        }
        __typename
      }
      __typename
    }
    lastUpdated
  }`,
};
