const gql = require('graphql-request');
const request = gql.request;

const { gqlGetHomeInfo } = require('./gql_getHomeInfo');

request('https://api.rdvn.page/graphql', gqlGetHomeInfo.query)
  .then(
    data => console.log(JSON.stringify(data, null, 2))
  )
  .catch(
    error => console.log(error)
  );
