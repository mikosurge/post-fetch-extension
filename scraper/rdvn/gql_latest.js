const gqlLatest = {
  operationName: "prepare",
  variables: {
    first: 9,
  },
  query: `query prepare($first: Int) {
    newestPosts: posts(orderBy: created_time_DESC, first: $first) { edges {
        node {
          r
          id
          raw_id
          comments_count
          message
          created_time
          likes_count
          attachments
          is_deleted
          status
          is_hide
          user {
            id
            raw_id
            name
            profile_pic
            nickname
            bio
            __typename
          }
          __typename
        }
        __typename
      }
      __typename
    }
  }`,
};
