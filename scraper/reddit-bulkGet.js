const gql = require('graphql-request');
const request = gql.request;
const fs = require('fs');

// const subreddits = [
//   'askhistorians',
//   'askreddit',
//   'askscience',
//   'books',
//   'breakups',
//   'coolguides',
//   'explainlikeimfive',
//   'lifeprotips',
//   'nonreddit',
//   'nostupidquestions',
//   'poems',
//   'science',
//   'space',
//   'tifu',
//   'todayilearned',
//   'unpopularopinion',
//   'unresolvedmysteries',
//   'unsentletters',
//   'writingprompts',
// ];

let bulkData = fs.readFileSync('../rdvn/subsALL.json');
let bulkJson = JSON.parse(bulkData);
let subreddits = bulkJson.subreddits.edges.map(
  _e => _e.node.raw_id
);
subreddits = subreddits.slice(1481);

const createGraphQuery = (sub, after = null) => (`query {
  posts(first: 100, orderBy: likes_count_DESC, after: ${after ? "\"" + after + "\"" : "null"}, filter: {r: "${sub}", since: null, until: null}) {
    pageInfo {
      hasNextPage
      endCursor
      __typename
    }
    totalCount
    edges {
      node {
        r
        id
        raw_id
        comments_count
        message
        created_time
        likes_count
        attachments
        is_deleted
        status
        is_hide
        user {
          id
          raw_id
          name
          profile_pic
          nickname
          bio
          __typename
        }
        __typename
      }
      __typename
    }
    __typename
  }
}`);

const sleep = seconds => {
  return new Promise((resolve, reject) => {
    setTimeout(
      () => {
        console.log(`Sleeping for ${seconds} seconds.`);
        resolve(true);
      }, seconds * 1000)
  });
};

const rdvnEndpoint = 'https://api.rdvn.page/graphql';

const getHundredRecords = async (endpoint, sub, after = null) => {
  return await request(endpoint, createGraphQuery(sub, after));
};

const getSubRecords = async (endpoint, sub) => {
  let index = 0;
  let total = 0;
  let fetched = 0;
  let currentResponse = await getHundredRecords(endpoint, sub);
    fs.writeFileSync(`./records/${sub}_${index}.json`, JSON.stringify(currentResponse, null, 2));

  let stillHasRecords = response => {
    let hasContent = response && response.posts && response.posts.pageInfo;
    if (!hasContent) { return false; };
    let pageInfo = response.posts.pageInfo;

    if (!pageInfo || !pageInfo.hasNextPage) { return false; }

    if (!pageInfo.endCursor) {
      console.log(`[${sub}] has next page, but no end cursor found!`);
      return false;
    }

    return true;
  };

  if (currentResponse.posts && currentResponse.posts.totalCount) {
    total = currentResponse.posts.totalCount;
    fetched += currentResponse.posts.edges.length;
  }

  while (stillHasRecords(currentResponse)) {
    console.log(`[${sub}] has more than 100 records`);
    let endCursor = currentResponse.posts.pageInfo.endCursor;
    index = index + 1;
    currentResponse = await getHundredRecords(endpoint, sub, endCursor);
    fs.writeFileSync(`./records/${sub}_${index}.json`, JSON.stringify(currentResponse, null, 2));

    if (currentResponse.posts && currentResponse.posts.totalCount) {
      total = currentResponse.posts.totalCount;
      fetched += currentResponse.posts.edges.length;
    }

    await sleep(1);
  }

  console.log(`Fetched [${fetched} / ${total}] of subreddit [${sub}]`);
};

const getAllSubReddits = async (endpoint, subreddits) => {
  for (let i = 0; i < subreddits.length; i++) {
    let sub = subreddits[i];

    await getSubRecords(endpoint, sub);
    await sleep(1);
  }
};

const main = () => {
  getAllSubReddits(rdvnEndpoint, subreddits)
    .then(
      () => console.log('Done getting all subreddits')
    )
    .catch(
      error => console.log(error)
    );
};

main();
