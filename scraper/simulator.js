const fs = require('fs');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;

const html = fs.readFileSync('./samples/index.html', { encoding: 'utf8' });
const dom = new JSDOM(html);
const $ = (require('jquery'))(dom.window);

// Add your logic here
